const fs = require("fs");
change(
	`<script type="module" src="main.js"></script>`,
	`<script src="generated.main.js"></script>`,
	`public/index.html`);
change(
	`Worker('generated.worker.js',{type:"module"})`,
	`Worker('generated.worker.js')`,
	`public/generated.main.js`);

function change(thisString,withThisString,inThisFile) {
	try{fs.writeFileSync(inThisFile,(fs.readFileSync(inThisFile,"utf8"))
			.replace(thisString,withThisString)
		,"utf8");} catch (e) {console.error(e);}

}
