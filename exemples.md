Alice à perdu l'accès à son compte.

Elle se rend dans l'annuaire cesium pour trouver sa clef publique (pour l'exemple ce sera `BsupQ354BitJsgn7ZLKfjS8zpgcRcskRzx5bbtHxWHQS` ) puis sur son ordinateur (Gsper n'est pas compatible mobile actuellement), elle l'ajoute dans le champ adéquoit de Gsper.

Elle sais qu'elle a utilisée un de ses emails comme identifiant secret, mais elle à plusieurs boites email qu'elle utilise courament. 
Elle les liste donc, 1 par ligne, dans la colonne de gauche (Liste d'identifiants secrets à tester) de Gsper :
```
contact@alice.fr
alice@gafam.com
perso@alice.fr
g1@alice.fr
```

En mot de passe, elle sais qu'elle s'est basée sur la phrase : `Alice et Bob sont plus libre grace à la Ǧ1.`

Mais elle à des doutes sur plusieurs points :
- a-t-elle utilisé des majuscule pour les prénoms ? pour Ǧ1 ?
- a-t-elle accentué "à" ? "grâce" ? Ǧ1 ?
- a-t-elle mis des espace entre les mots, ou des majuscule à chaque mot, ou tout attaché en minuscule ?
- a-t-elle fait des fautes ou mis au futur ? (et/est), (son/sont/seront/serons)
- a-t-elle mis un point d'exclamation, un point, ou rien du tout ?

Elle va donc commencer simplement à mettre en seconde colonne (Liste de mot de passe à tester) les phrases :
```
Alice et Bob sont plus libre grâce à la Ǧ1
Alice et Bob sont plus libre grâce à la G1
Alice et Bob sont plus libre grace à la Ǧ1
Alice et Bob sont plus libre grace à la G1

Alice et Bob sont plus libre grâce à la Ǧ1.
Alice et Bob sont plus libre grâce à la G1.
Alice et Bob sont plus libre grace à la Ǧ1.
Alice et Bob sont plus libre grace à la G1.

Alice et Bob sont plus libre grâce à la Ǧ1 !
Alice et Bob sont plus libre grâce à la G1 !
Alice et Bob sont plus libre grace à la Ǧ1 !
Alice et Bob sont plus libre grace à la G1 !

Alice et Bob seront plus libre grâce à la Ǧ1
Alice et Bob seront plus libre grâce à la G1
Alice et Bob seront plus libre grace à la Ǧ1
Alice et Bob seront plus libre grace à la G1

Alice et Bob seront plus libre grâce à la Ǧ1.
Alice et Bob seront plus libre grâce à la G1.
Alice et Bob seront plus libre grace à la Ǧ1.
Alice et Bob seront plus libre grace à la G1.

Alice et Bob seront plus libre grâce à la Ǧ1 !
Alice et Bob seront plus libre grâce à la G1 !
Alice et Bob seront plus libre grace à la Ǧ1 !
Alice et Bob seront plus libre grace à la G1 !
```
Puis elle clique sur "Désaccentuer" et "MaJusculeS" de la colonne de droite (mot de passe) pour générer les variantes totalement désaccentuées, avant de cliquer sur "Gsper" pour lancer les essais et essayer de retrouver les bons identifiants et mot de passe.

Les 480 combinaisons sont testées, mais sans succès.
Avant de perdre espoir, Alice s'essaie au mode avancé.
Elle indique donc dans la clonne de droite ceci (après lecture attentive de la documentation et plusieurs essai de syntaxe) :
```
[Aa]lice e(s|)t [Bb]ob (son(t|)|seron[ts]) plus libre gr[âa]ce [àa] la ([ǦGǧg]1|[Jj]une)(.| !|)
[Aa]liceE(s|)tBob(Son(t|)|Seron[ts])PlusLibreGr[âa]ce[AÀà]La([ǦG]1|June)(.|!|)
alicee(s|)tbob(son(t|)|seron[ts])pluslibregr[âa]ce[aà]la([Ǧǧg]1|june)(.|!|)
```
puis clique sur "&lt;({[Avancé]})&gt;" pour générer les combinaisons : 14208 cas à tester, et lance en cliquant sur "Gsper".

Plusieurs minutes plus tard, Gsper à trouvé ! (mais je ne vous spoile pas la réponse, essayez avec la clef publique d'exemple, vous devriez trouver comme Alice, l'identifiants secret et le mot de passe correspondant au compte d'Alice.)

PS : Cet exemple outre servir de guide d'utilisation apporte deux éclairage :
- si vous utilisez des identifiants secrets et mot de passe tellement simple qu'une recherche générique les trouve, n'importe qui peu accéder à votre compte, il n'est donc pas sécurisé.
- si vous utilisez des identifiants secrets et mot de passe suffisement imprédictible, il sont introuvable sans en avoir un souvenir, au moins approximatif, donc introuvable par autruit.


