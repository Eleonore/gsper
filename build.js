const fs = require("fs");
try{fs.rmdirSync("public/vendors");} catch (e) {console.error(e);}
try{fs.mkdirSync("public/vendors");} catch (e) {console.error(e);}
try{fs.copyFileSync("node_modules/tweetnacl/nacl-fast.js","public/vendors/nacl.js");} catch (e) {console.error(e);}
try{fs.copyFileSync("node_modules/scrypt-async-modern/src/index.js","public/vendors/scrypt.js");} catch (e) {console.error(e);}
try{fs.writeFileSync("public/vendors/nacl.js",(fs.readFileSync("public/vendors/nacl.js","utf8"))
			.replace("(function(nacl) {","var nacl = {};")
			.replace("})(typeof module !== 'undefined' && module.exports ? module.exports : (self.nacl = self.nacl || {}));","export {nacl};")
		,"utf8");} catch (e) {console.error(e);}

