import * as app from "./dictionaryBuilder.js";
describe('dictionaryBuilder', () => {
	beforeEach(app.resetCache);
	it('add no accents variant', () => {
		expect(app.noAccentVariant("Ǧ1")).toEqual(["Ǧ1","G1"]);
	});
	it('add case variants', () => {
		expect(app.casesVariants("moT")).toEqual(["moT","mot","Mot","MOT"]);
	});
	it('add multi word case variants', () => {
		expect(app.casesVariants("autre mot")).toEqual(["autre mot","Autre Mot","AUTRE MOT"]);
	});
	it('regLikeVariants remove ref:: lines', () => {
		expect(app.regLikeVariants("ref::truc")).toEqual([]);
	});
    it('regLikeVariants handle <ref>', () => {
        expect(app.regLikeVariants("<ref> <ref>",["ref::truc","ref::bidule","<ref> <ref>"])).toEqual(["truc truc","bidule truc", "truc bidule", "bidule bidule"]);
        expect(app.regLikeVariants("<ref> <ref>",["ref::(truc|bidule)","<ref> <ref>"])).toEqual(["truc truc","bidule truc", "truc bidule", "bidule bidule"]);
    });
    it('regLikeVariants handle =ref>', () => {
        expect(app.regLikeVariants("=ref> =ref>",["ref::truc","ref::bidule","=ref> =ref>"])).toEqual(["truc truc","bidule bidule"]);
        expect(app.regLikeVariants("=ref> =ref>",["ref::(truc|bidule)","=ref> =ref>"])).toEqual(["truc truc","bidule bidule"]);
    });
    it('regLikeVariants handle multiple =ref>', () => {
        expect(app.regLikeVariants("=ref> =ref2> =ref> =ref2>",
			["ref::(truc|bidule)","ref2::(machin|chose)","=ref> =ref>"]
		)).toEqual(["truc machin truc machin","bidule machin bidule machin","truc chose truc chose","bidule chose bidule chose"]);
    });
	it('regLikeVariants handle (this|that)', () => {
		expect(app.regLikeVariants("(this|that)")).toEqual(["this","that"]);
	});
	it('regLikeVariants handle [ -_]', () => {
		expect(app.regLikeVariants("[ -_]")).toEqual([" ","-","_"]);
	});
	it('regLikeVariants handle [a-f]', () => {
		expect(app.regLikeVariants("[a-f]")).toEqual(["a","b","c","d","e","f"]);
		expect(app.regLikeVariants("[7-9]")).toEqual(["7","8","9"]);
		expect(app.regLikeVariants("[C-F]")).toEqual(["C","D","E","F"]);
	});
	it('regLikeVariants handle [a-c-]', () => {
		expect(app.regLikeVariants("[a-c-]")).toEqual(["a","b","c","-"]);
	});
	it('regLikeVariants handle {qty}', () => {
		expect(app.regLikeVariants("a{5}")).toEqual(["aaaaa"]);
	});
	it('regLikeVariants handle {min,max}', () => {
		expect(app.regLikeVariants("b{3,5}")).toEqual(["bbb","bbbb","bbbbb"]);
	});
	it('regLikeVariants handle (string){qty}', () => {
		expect(app.regLikeVariants("c'est (toto|tata){0,2}")).toEqual(["c'est ","c'est toto","c'est totototo","c'est tata","c'est tatatoto","c'est tototata","c'est tatatata"]);
	});
	it('regLikeVariants handle nested -([a-f]|<ref>){0,1}', () => {
		expect(app.regLikeVariants("-([B-D]|<ref>){0,1}",["ref::plop"])).toEqual(["-","-B","-plop","-C","-D"]);
	});
	it('regLikeVariants handle plop:\\:', () => {
		expect(app.regLikeVariants("plop:\\:ici")).toEqual(["plop::ici"]);
		expect(app.regLikeVariants("plop\\::ici")).toEqual(["plop::ici"]);
		expect(app.regLikeVariants("plop::ici")).toEqual([]);
	});
	it('regLikeVariants handle [\\]*]', () => {
		expect(app.regLikeVariants("[\\]*]")).toEqual(["]","*"]);
	});
});
