const idSec = "a";
const mdp = "b";
//base58
const pubKey = "AoxVA41dGL2s4ogMNdbCw3FFYjFo5FPK36LuiW1tjGbG";
const secretKey = "3ZsmZhnRv137dS1s7Q3jFGKLTDyhkwguPHfnWBxzDCTTHKWGnYw9zBk3gcCUJCc72TEUuyzM7cqpo7c5LYhs1Qtv";
const seed = "9eADqX8V6VcPdJCHCVYiE1Vnift9nFNrvr9aTaXA5RJc";

import * as app from "./crypto.js";
describe('crypto', () => {
	it('b58 should decode/encode well', () => {
		expect(app.b58.encode(app.b58.decode(pubKey))).toEqual(pubKey);
	});
	it('saltPass2seed should convert salt & password to seed with scrypt', async () => {
		expect(app.b58.encode(await app.saltPass2seed(idSec,mdp))).toEqual(seed);
	});
	it('seed2keyPair should generate public and private key nacl/sodium way.', async () => {
		const rawSeed = app.b58.decode(seed);
		const rawKeyPair = app.seed2keyPair(rawSeed);
		expect(app.b58.encode(rawKeyPair.publicKey)).toEqual(pubKey);
		expect(app.b58.encode(rawKeyPair.secretKey)).toEqual(secretKey);
	});
	it('idSecPass2cleanKeys should output clean base58 keys and seed', async () => {
		const res = await app.idSecPass2cleanKeys(idSec,mdp);
		expect(res.publicKey).toEqual(pubKey);
		expect(res.secretKey).toEqual(secretKey);
		expect(res.seed).toEqual(seed);
		expect(res.idSec).toEqual(idSec);
		expect(res.password).toEqual(mdp);
	});

});


