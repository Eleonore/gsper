import * as crypto from "./crypto.js";

self.addEventListener('message', async function(event){
	if(event.data === 'stop') self.close();

	const keys = await crypto.idSecPass2cleanKeys(event.data.idSec,event.data.pass);
	if(event.data.pub === keys.publicKey){
		keys.match = true;
	}
	postMessage(keys);
});
