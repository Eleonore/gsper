import * as dico from "./dictionaryBuilder.js";

const workers = [];
function startWorkers() {
	for (let i=0;i<navigator.hardwareConcurrency;i++){
		workers[i] = new Worker('generated.worker.js',{type:"module"}); // en attendant un meilleur support des modules et import
		workers[i].addEventListener('message', combiTested);
	}
}
startWorkers();
let combi;
let tested = 0;
let departTime;
let lastDisplayUpdate=Date.now();
function combiTested(event){
	tested++;
	const now = Date.now();
	if(now-lastDisplayUpdate>100){
		lastDisplayUpdate=now;
		console.log(event.data);
		document.getElementById("percent").innerHTML = `${Math.round(10000*tested/combi)/100}%`;
		const combiPerSec = tested/((now-departTime)/1000);
		document.getElementById("temps").innerHTML = `${Math.round((combi-tested)/combiPerSec)}s (${Math.round(combiPerSec)}/s)`;
	}
	if(event.data.match){
		document.getElementById("resultat").classList.add("active");
		document.getElementById("resIdSec").innerText = event.data.idSec;
		document.getElementById("resPass").innerText = event.data.password;
		document.getElementById("resPublicKey").innerText = event.data.publicKey;
		document.getElementById("resSecretKey").innerText = event.data.secretKey;
		document.getElementById("resSeed").innerText = event.data.seed;
		stopNow();
	}
	if(tested === combi) {
		document.getElementById("percent").innerHTML = "Fini sans résultats";
        document.getElementById("fini").classList.add("active");
        idleStateBtn();
	}
}

function genAction(event,func){
	const textarea = event.target.parentNode.parentNode.querySelector("textArea");
	dico.resetCache();
	textarea.value = dico.dedup(apply(multiLineString2cleanArray(textarea.value),[func])).join("\n");
	updateEstimate();
}

addEventsListeners(document.querySelectorAll(".accent"),"click",(e)=>genAction(e,dico.noAccentVariant));
addEventsListeners(document.querySelectorAll(".case"),"click",(e)=>genAction(e,dico.casesVariants));
addEventsListeners(document.querySelectorAll(".regLike"),"click",(e)=>genAction(e,dico.regLikeVariants));




addEventsListeners(document.querySelectorAll("#salt, #pass"),"change keyup",updateEstimate);
updateEstimate();
function updateEstimate(){
	const idSecList = multiLineString2cleanArray(document.getElementById("salt").value);
	const passList = multiLineString2cleanArray(document.getElementById("pass").value);
	document.getElementById("combi").innerHTML = idSecList.length*passList.length;
	document.getElementById("temps").innerHTML = "&lt; "+(idSecList.length*passList.length/10)+"s";
}
function idleStateBtn(){
	document.getElementById("compute").innerText = "Gsper !";
	document.getElementById("compute").classList.remove("abort");
}
function workingStateBtn() {
	document.getElementById("compute").innerText = "Stop !";
	document.getElementById("compute").classList.add("abort");
}
function stopNow(){
	idleStateBtn();
	for (let i=0;i<navigator.hardwareConcurrency;i++) workers[i].terminate();
	setTimeout(startWorkers,0);
	tested = 0;
}
document.getElementById("compute").addEventListener("click", e=>{
	const pub = document.getElementById("pubkey").value.trim();
	if(!pub) return document.getElementById("noPubKey").classList.add("active");
	const idSecList = multiLineString2cleanArray(document.getElementById("salt").value);
	const passList = multiLineString2cleanArray(document.getElementById("pass").value);
	combi = idSecList.length*passList.length;
	if(tested<combi && tested>0) return stopNow();
	workingStateBtn();
	departTime = Date.now();
	updateEstimate();
	document.getElementById("percent").innerHTML = "0%";
	tested = 0;
	let count = 0;
	for(let idSec of idSecList){
		for (let pass of passList){
			workers[count%workers.length].postMessage({pub,idSec,pass});
			count++;
		}
	}
});
function addEventsListeners(triggerNodes,events,functions){
	if(!triggerNodes.length) triggerNodes = [triggerNodes];
	if(typeof events !== "object") events = events.split(" ");
	if(typeof functions !== "object") functions = [functions];
	for(let n of triggerNodes) events.forEach(e=> functions.forEach(f=>n.addEventListener(e,f)));
}
function multiLineString2cleanArray(rawStr){ return rawStr.split("\n").map(str=>str.trim()).filter(str=>str !== ""); }




function apply(data,funcList){
	let oldData = JSON.parse(JSON.stringify(data));
	for (let f of funcList){
		let nextData = [];
		for (let str of oldData){
			nextData = nextData.concat(f(str,oldData));
		}
		oldData = nextData;
	}
	return oldData;
}



/* pop-in handling */
function bgCloseBuilder(side){
    const bgClose = document.createElement("a");
    bgClose.classList.add(`bg${side}`);
    bgClose.classList.add("bgPopupClose");
    bgClose.href = "#";
    bgClose.title = "Fermer";
    return bgClose;
}
document.querySelectorAll('.subPage').forEach( popup => {
    const fermer = document.createElement("a");
    fermer.classList.add("close");
    fermer.href = "#";
    fermer.title = "Fermer";
    fermer.innerHTML = "X";
    popup.appendChild(fermer);
    popup.appendChild(bgCloseBuilder('Top'));
    popup.appendChild(bgCloseBuilder('Left'));
    popup.appendChild(bgCloseBuilder('Right'));
    popup.appendChild(bgCloseBuilder('Bottom'));
} );
document.querySelectorAll('a[href^="#"]').forEach(
    link => link.addEventListener('click',
        e => {
            document.querySelectorAll('.subPage').forEach( section => section.classList.remove("active") );
            e.preventDefault();
            return false;
        }
    )
);
