# Gsper
Ǧ'Perdu l'accès à mon compte, Ǧ'spère le retrouver !

### [Ǧ'vais essayer maintenant !](https://gsper.duniter.io)
Ǧ'vais essayer hors-ligne : télécharger Ǧsper en [zip](https://gsper.duniter.io/gsper-offline.zip) ou [tgz](https://gsper.duniter.io/gsper-offline.tgz).

## Principe
- [x] Gsper test pour les combinaisons d'identifiants secrets et de mot de passe
et vous signal si une des correspondance correspond au compte recherché.
- [x] Il génère aussi des variantes issue des mots que vous lui fournissez
pour vous donner plus de changes de trouver la bonne combinaison.
- [x] Il affiche le nombre de combinaisons à tester, et le temps estimé pour les essayer toutes.

En sécurité informatique ce que des outils tel que Gsper font est appellé [attaque par dictionnaire](https://fr.wikipedia.org/wiki/Attaque_par_dictionnaire).

## Financer le développement

- Page de crowdfunding sur gannonce (bientôt)
- Compte dédié au projet : https://cesium.ǧ1.money/#/app/wot/22TTh6fD8uEC9CYq5sB8QpqjyXEHH5QnAiEdy68D2Y7S/gsper
- Faire un don : [150,30 Ǧ1 (15 min)](https://cesium.ǧ1.money/api/#/v1/payment/22TTh6fD8uEC9CYq5sB8QpqjyXEHH5QnAiEdy68D2Y7S?name=Financement%20de%20Gsper&amount=150.30&comment=REFERENCE),
[601,20 Ǧ1 (1h)](https://cesium.ǧ1.money/api/#/v1/payment/22TTh6fD8uEC9CYq5sB8QpqjyXEHH5QnAiEdy68D2Y7S?name=Financement%20de%20Gsper&amount=601.2&comment=REFERENCE),
[3006 Ǧ1 (6h)](https://cesium.ǧ1.money/api/#/v1/payment/22TTh6fD8uEC9CYq5sB8QpqjyXEHH5QnAiEdy68D2Y7S?name=Financement%20de%20Gsper&amount=3006&comment=REFERENCE)

Le temps entre parentèse est l'équivalent temps de dev au tarifs indiqué sur le site du développeur : https://1forma-tic.fr

## Guide d'utilisation

1. Indiquer la clef publique de votre compte (si vous ne la connaissez pas, chercher votre profil sur [cesium](https://cesium.ǧ1.money/#/app/wot/lg) via la fonction recherche de l'annuaire)
2. Indiquer les variantes d'identifiant secret que vous souhaitez tester. (Il est possible de générer plus de variantes automatiquement. [La documentation](#Documentation) ci dessous explique comment.)
3. Indiquer les variantes de mot de passe que vous souhaitez tester.
4. Cliquer sur le bouton `Gsper !` pour lancer la tentative de récupération des accès au compte.
5. Patienter jusqu'à l'apparition du message de fin de recherche (avec ou sans succès).

[Exemples d'utilisation pas à pas](exemples.md)

## Documentation
Gsper met à votre disposition plusieurs boutons pour générer des variantes à tester à partir de celles que vous avez indiqué.
Voici comment les utiliser.

### Bouton `MaJusculeS` 
Génère des variantes des combinaisons à tester :
- entièrement en majuscule
- entièrement en minuscule
- avec le premier caractère en majuscule uniquement
- tel quel, sans modifier les majuscule et minuscule

**Exemple :** 
```
moT
```
sera remplacé par :
```
mot
MOT
Mot
moT
```
### Bouton `désaccentué` 
Génère la variante désaccentué des combinaisons à tester.

**Exemple :** 
```
Gavé de Ǧ1
```
sera remplacé par :
```
Gavé de Ǧ1
Gave de G1
```

### Bouton `<({[Avancé]})>` 

Génère des variantes avancées selon un syntaxe spécifique.

<!-- **/!\ Attention /!\\** Générer un trop grand nombre de combinaison à tester peut faire planter le navigateur, et mettre des année à tester de toute façon. --> 

Détail de la syntaxe :

###### `<référence>` remplace `<référence>` par les lignes commançant par `référence::`
**Exemple :** 
```
mot&lt;space&gt;de&lt;space&gt;passe
space::-
space::_
```
sera remplacé par :
```
mot-de-passe
mot-de_passe
mot_de-passe
mot_de_passe
```

###### `=référence>` remplace de façon syncroniser pluiseurs `=référence>` par la meme variante des lignes référencées par `référence::`
**Exemple :** 
```
mot=space&gt;de=space&gt;passe
space::-
space::_
```
sera remplacé par :
```
mot-de-passe
mot_de_passe
```

###### `(variante|autre variante)` décline les séquences listées comme des combinaisons différentes.
**Exemple :** 
```
(mot|phrase) de passe
```
sera remplacé par :
```
mot de passe
phrase de passe
```

###### `[a-z§¤]` décline les caractères listés comme des combinaisons différentes.
**Exemple :** 
```
J'ai oublié la suite[!.]
```
sera remplacé par :
```
J'ai oublié la suite!
J'ai oublié la suite.
```

**Exemple :** 
```
Spam [0-3]
```
sera remplacé par :
```
Spam 0
Spam 1
Spam 2
Spam 3
```

###### `A{0,2}` décline l'élément précédent selon les quantités définies.
**Exemple :** 
```
c'est (mat|tic){0,3}
```
sera remplacé par :
```
c'est 
c'est mat
c'est matmat
c'est matmatmat
c'est matmattic
c'est mattic
c'est matticmat
c'est mattictic
c'est tic
c'est ticmat
c'est ticmatmat
c'est ticmattic
c'est tictic
c'est ticticmat
c'est tictictic
```

###### `\ ` permet de banaliser le caractère suivant
**Exemple :** 
```
un (am\||\|nconu)
```
sera remplacé par :
```
un am|
un |nconu
```
**Contre exemple :**
```
un (am|||nconu)
```
sera remplacé par :
```
un am
un 
un 
un nconu
```
